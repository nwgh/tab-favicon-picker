// This is more involved than our regular favicon updater, since we don't
// want to hit ALL gmail tabs, just the one containing the Mozilla email

// We figure out if we're on the proper gmail instance by checking the
// page title
(function () {
    var titles = top.document.getElementsByTagName("title");
    if (titles.length > 0) {
      var title = titles[0];
      if (title.childNodes.length > 0) {
          var titleNode = title.childNodes[0];
          if (titleNode.textContent.length > 0) {
              var titleText = titleNode.textContent;
              if (titleText.search(/Mozilla.+Mail/) != -1) {
                  updateFavicon("https://planet.mozilla.org/img/mozilla-16.png");
              }
          }
      }
    }
})();
