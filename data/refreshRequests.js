var refreshers = document.querySelectorAll(".refresh");
var timeout = 5 * 60 * 1000; // 5 minutes

function doRefresh() {
  for (var i = 0; i < refreshers.length; ++i) {
    refreshers[i].click();
  }
  setTimeout(doRefresh, timeout);
}

setTimeout(doRefresh, timeout);
