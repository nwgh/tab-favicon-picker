updateFavicon = function(href) {
  // Remove any existing favicon
  try {
    var head = top.document.getElementsByTagName("head")[0];
  }
  catch (e) {
    console.log("couldn't get heads");
    return;
  }

  console.log("going through links");
  var links = head.getElementsByTagName("link");
  for (var i = 0 ; i < links.length ; i++) {
    var link = links[i];
    if (link.rel.toLowerCase() == "shortcut icon") {
      head.removeChild(link);
    }
  }

  // Insert the new favicon using the URL we were given
  console.log("adding new favicon");
  var favicon = top.document.createElement("link");
  favicon.rel = "shortcut icon";
  favicon.href = href;
  head.appendChild(favicon);
}
