var pageMod = require("sdk/page-mod");
//var prefService = require("preferences-service");
var self = require("sdk/self");

// Hook up tab watcher for Gmail location
// Listen for any Gmail tabs to be opened and hook up the Inbox or
// new items watchers to update the favicon when the number of unreads changes
pageMod.PageMod({
  include: ["https://www.fastmail.com/calendar/*"],
  attachTo: ["existing", "top"],
  contentScriptWhen: "end",
  contentScriptFile: [self.data.url("updateFavicon.js"),
                      self.data.url("calendarFavicon.js")]
});

pageMod.PageMod({
  include: ["https://bugzilla.mozilla.org/*"],
  attachTo: ["existing", "top"],
  contentScriptWhen: "end",
  contentScriptFile: [self.data.url("hideBugzillaBadge.js")]
});

pageMod.PageMod({
  include: ["https://www.fastmail.com/notes/*"],
  attachTo: ["existing", "top"],
  contentScriptWhen: "end",
  contentScriptFile: [self.data.url("updateFavicon.js"),
                      self.data.url("notesFavicon.js")]
});

pageMod.PageMod({
  include: ["https://bugzilla.mozilla.org/page.cgi?id=mydashboard.html"],
  attachTo: ["existing", "top"],
  contentScriptWhen: "end",
  contentScriptFile: [self.data.url("refreshRequests.js")]
});

pageMod.PageMod({
  include: ["https://mail.google.com/*"],
  attachTo: ["existing", "top"],
  contentScriptWhen: "end",
  contentScriptFile: [self.data.url("updateFavicon.js"),
                      self.data.url("mozmailFavicon.js")]
});
